﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using XQ.SIRE.GeoLocations.DAL.DbModels;
using XQ.SIRE.GeoLocations.DAL.Repositories.Abstracts;

namespace XQ.SIRE.GeoLocations.WebService.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CountryController : Controller
    {
        private readonly ICountriesRepository _countriesRepository;
        private readonly ILogger<CountryController> _logger;

        public CountryController(ICountriesRepository countriesRepository, ILogger<CountryController> logger)
        {
            _countriesRepository = countriesRepository ?? throw new ArgumentNullException(nameof(countriesRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        //Get api/v<version number>/country
        /// <summary>
        /// Get all countries
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var allCountries = await _countriesRepository.GetAll();
            return Ok(allCountries);
        }

        //Get api/v<version number>/country/<country id>
        /// <summary>
        /// Get country info by identifier
        /// </summary>
        /// <param name="countryId">Country identifier</param>
        /// <returns></returns>
        [HttpGet("{countryId}")]
        public async Task<IActionResult> GetById(string countryId)
        {
            Country countryInfo;

            try
            {
                countryInfo = await _countriesRepository.GetById(countryId);
            }
            catch (InvalidCastException e)
            {
                _logger.LogError(default(EventId), e, e.Message);
                return BadRequest($"Invalid input country identifier with value - {countryId}");
            }

            return Ok(countryInfo);
        }

        //Get api/v<version number>/country/getByName/<country name part>
        /// <summary>
        /// Get first 20 countries that contains in international name part of input value
        /// NOTE: min length of input value is 2 digits
        /// </summary>
        /// <param name="countryNamePart">Input country name part</param>
        /// <returns>Collection of found countries</returns>
        [HttpGet("getbyname/{countryNamePart:minlength(2)}")]
        public async Task<IActionResult> GetByName(string countryNamePart)
        {
            ICollection<Country> foundedCountries;

            if (string.IsNullOrEmpty(countryNamePart))
                return BadRequest("Input country name can not be empty");

            try
            {
                foundedCountries = await _countriesRepository.GetByNamePart(countryNamePart);
            }
            catch (ArgumentNullException e)
            {
                _logger.LogError(default(EventId), e, e.Message);
                return BadRequest($"Filter should be defined for searching of countries by input value");
            }

            return Ok(foundedCountries);
        }
    }
}