﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using XQ.SIRE.GeoLocations.DAL.DbModels;
using XQ.SIRE.GeoLocations.DAL.Repositories.Abstracts;
using XQ.SIRE.GeoLocations.WebService.Controllers;
using Xunit;

namespace XQ.SIRE.GeoLocations.WebService.Tests.Controllers
{
    public class CountryControllerTests
    {
        #region Ctor tests

        [Fact]
        public void Ctor_Throws_CountriesRepository_ArgumentNullException()
        {
            var logger = new Mock<ILogger<CountryController>>();

            //Assert
            var ex = Assert.Throws<ArgumentNullException>(() => new CountryController(null, logger.Object));
            Assert.Equal("countriesRepository", ex.ParamName);
        }

        [Fact]
        public void Ctor_Throws_Logger_ArgumentNullException()
        {
            var rep = new Mock<ICountriesRepository>();

            //Assert
            var ex = Assert.Throws<ArgumentNullException>(() => new CountryController(rep.Object, null));
            Assert.Equal("logger", ex.ParamName);
        }

        #endregion

        #region Get tests

        [Fact]
        public async Task Get_Returns_Countries_Collection()
        {
            //Arrange
            var logger = new Mock<ILogger<CountryController>>();
            var countriesRepository = new Mock<ICountriesRepository>();
            countriesRepository.Setup(x => x.GetAll()).ReturnsAsync(GetCountriesCollection);

            //Act
            var controller = new CountryController(countriesRepository.Object, logger.Object);
            var result = await controller.Get();

            //Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);
            var collectionResult = Assert.IsAssignableFrom<ICollection<Country>>(okResult.Value);
            Assert.Equal(3, collectionResult.Count);
        }

        #endregion

        #region GetById tests

        [Fact]
        public async Task GetById_Returns_BadRequest_For_Incorrect_CountryId()
        {
            //Arrange
            string invalidCountryId = "324552_invalid_id_dsgg454";
            var logger = new Mock<ILogger<CountryController>>();
            var countriesRepository = new Mock<ICountriesRepository>();
            countriesRepository.Setup(x => x.GetById(It.IsAny<string>())).ThrowsAsync(new InvalidCastException());

            //Act
            var controller = new CountryController(countriesRepository.Object, logger.Object);
            var result = await controller.GetById(invalidCountryId);

            //Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(StatusCodes.Status400BadRequest, badRequestResult.StatusCode);
            Assert.Equal($"Invalid input country identifier with value - {invalidCountryId}", badRequestResult.Value);
        }

        [Fact]
        public async Task GetById_Returns_OkResult_With_One_Country()
        {
            //Arrange
            string countryId = "3276ad26-5b10-4ebf-a3d7-a381a1e4babc";
            var logger = new Mock<ILogger<CountryController>>();
            var countriesRepository = new Mock<ICountriesRepository>();
            countriesRepository.Setup(x => x.GetById(It.IsAny<string>())).ReturnsAsync(GetSingleCountry);

            //Act
            var controller = new CountryController(countriesRepository.Object, logger.Object);
            var result = await controller.GetById(countryId);

            //Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);
            var countryValue = Assert.IsAssignableFrom<Country>(okResult.Value);
            Assert.Equal(countryId, countryValue.Id);
        }

        #endregion

        #region GetByName tests

        [Fact]
        public async Task GetByName_Returns_BadRequest_With_Empty_CountryNamePart()
        {
            var logger = new Mock<ILogger<CountryController>>();
            var countriesRepository = new Mock<ICountriesRepository>();

            var countryController = new CountryController(countriesRepository.Object, logger.Object);
            var result = await countryController.GetByName(string.Empty);

            var badResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(StatusCodes.Status400BadRequest, badResult.StatusCode);
            Assert.Equal("Input country name can not be empty", badResult.Value);
        }

        [Fact]
        public async Task GetByName_Returns_BadRequest_When_Catch_ArgumentNullException_For_Empty_CountryId()
        {
            var logger = new Mock<ILogger<CountryController>>();
            var countriesRepository = new Mock<ICountriesRepository>();
            countriesRepository.Setup(x => x.GetByNamePart(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException());

            var countryController = new CountryController(countriesRepository.Object, logger.Object);
            var result = await countryController.GetByName(" ");

            var badResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(StatusCodes.Status400BadRequest, badResult.StatusCode);
            Assert.Equal("Filter should be defined for searching of countries by input value", badResult.Value);
        }

        [Fact]
        public async Task GetByName_Returns_One_Country_That_Contains_InputPartName()
        {
            var logger = new Mock<ILogger<CountryController>>();
            var countriesRepository = new Mock<ICountriesRepository>();
            countriesRepository.Setup(x => x.GetByNamePart(It.IsAny<string>())).ReturnsAsync(GetFoundCountriesByPartName);

            var countryController = new CountryController(countriesRepository.Object, logger.Object);
            var result = await countryController.GetByName("Ca");

            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, okResult.StatusCode);
            var foundCountriesResult = Assert.IsAssignableFrom<ICollection<Country>>(okResult.Value);
            Assert.Equal(1, foundCountriesResult.Count);
            Assert.Equal("ee24d87b-0b6a-4e0c-90e1-2fbecd8c97c7", foundCountriesResult.First().Id);
        }

        #endregion

        #region additional methods

        private Country GetSingleCountry()
        {
            var country = new Country
            {
                Id = "3276ad26-5b10-4ebf-a3d7-a381a1e4babc",
                CountryId = 1,
                LocalName = "Russia"
            };

            return country;
        }

        private ICollection<Country> GetFoundCountriesByPartName()
        {
            ICollection<Country> foundCountries = new List<Country>
            {
                new Country
                {
                    Id = "ee24d87b-0b6a-4e0c-90e1-2fbecd8c97c7",
                    CountryId = 2,
                    LocalName = "Canada"
                }
            };

            return foundCountries;
        }

        private ICollection<Country> GetCountriesCollection()
        {
            ICollection<Country> collection = new List<Country>()
            {
                new Country
                {
                    Id = "3276ad26-5b10-4ebf-a3d7-a381a1e4babc",
                    CountryId = 1,
                    LocalName = "Russia"
                },
                new Country
                {
                    Id = "ee24d87b-0b6a-4e0c-90e1-2fbecd8c97c7",
                    CountryId = 2,
                    LocalName = "Canada"
                },
                new Country
                {
                    Id = "1d199f60-c8a3-424a-b42c-f366ec07b866",
                    CountryId = 3,
                    LocalName = "USA"
                }
            };

            return collection;
        }

        #endregion
    }
}
