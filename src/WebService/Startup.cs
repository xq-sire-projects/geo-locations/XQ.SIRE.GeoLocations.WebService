﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using XQ.SIRE.GeoLocations.DAL.DbContext;
using XQ.SIRE.GeoLocations.DAL.Factories;
using XQ.SIRE.GeoLocations.DAL.Repositories;
using XQ.SIRE.GeoLocations.DAL.Repositories.Abstracts;
using XQ.SIRE.GeoLocations.WebService.Common.Extensions;

namespace XQ.SIRE.GeoLocations.WebService
{
    public class Startup
    {
        private readonly IConfiguration _configutation;

        public Startup(IConfiguration configutation)
        {
            _configutation = configutation;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //set versions for routes
            services.AddApiVersioning(opts =>
            {
                opts.DefaultApiVersion = new ApiVersion(new DateTime(2018, 5, 6));
                opts.AssumeDefaultVersionWhenUnspecified = true;
                opts.ReportApiVersions = true;
            });

            services.AddLogging();

            //set mongo db connection settings
            services.ConfigureMongoDbConnection(_configutation);

            services.AddScoped<IMongoDbFactory, GeoLocationsDbFactory>();
            services.AddScoped<ILocationsDbContext, LocationsDbContext>();

            #region DI repositories

            services.AddScoped<ICountriesRepository, CountriesRepository>();

            #endregion
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvcWithDefaultRoute();

            app.UseStatusCodePages();
        }
    }
}
