﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using XQ.SIRE.GeoLocations.DAL.Options;

namespace XQ.SIRE.GeoLocations.WebService.Common.Extensions
{
    public static class MongoConnectionConfiguration
    {
        /// <summary>
        /// Configurates mongo database connection settings
        /// </summary>
        /// <param name="services">service collection</param>
        /// <param name="configuration">Application configurations</param>
        public static void ConfigureMongoDbConnection(this IServiceCollection services, IConfiguration configuration)
        {
            //get all config settings from environment variables
            services.Configure<MongoDbConnectionSettings>(opts =>
            {
                opts.DatabaseName = configuration.GetValue<string>("SIRE_GEO_DB_NAME");
                opts.HostName = configuration.GetValue<string>("SIRE_GEO_HOST_NAME");
                opts.Port = configuration.GetValue<int>("SIRE_GEO_HOST_PORT");
                opts.UserName = configuration.GetValue<string>("SIRE_GEO_DB_USERNAME");
                opts.Password = configuration.GetValue<string>("SIRE_GEO_DB_PASSWORD");
            });
        } 
    }
}
