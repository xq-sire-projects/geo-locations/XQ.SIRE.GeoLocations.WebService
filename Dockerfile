FROM microsoft/dotnet:2.2-sdk AS build

MAINTAINER Denis Safonov <safonovd90@gmail.com>

EXPOSE 5000

# restore and publish web service with dependencies in folder 
COPY . /app
WORKDIR "/app/src/WebService"
RUN dotnet restore && dotnet publish -c Release -o out

#copy files (created after dotnet publish) form previous image into runtime alpine image 
FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build "/app/src/WebService/out" ./

CMD ["dotnet", "XQ.SIRE.GeoLocations.WebService.dll"]
